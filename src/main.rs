
use std::fs::OpenOptions;
use std::io::{Write, stdout, self, BufWriter};

use tournament_sim::structures::tournament::Tournament;



fn main() {
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open("output.txt").expect("open file error");

    let mut end_results = vec![];
    for rounds in 1..=30 {
        for losses in 1..=rounds {
            let mut tournaments = vec![];
            for _ in 0..20 {
                let mut tournament = Tournament::new(rounds, 100000, losses);
                tournament.run();
                tournaments.push(tournament);
            }
            output(&mut stdout(), tournaments.as_slice()).expect("error writing to stdout");
            let avg_yield = output(&mut file, tournaments.as_slice()).expect("error writing to file");

            end_results.push(format!("Rounds: {}, Losses To Eliminate: {}, Average Yield: {}",
                tournaments[0].round_num, tournaments[0].max_loss, avg_yield
            ));
        }
    }

    output_end_results(&mut stdout(), &end_results).expect("Error writing end results to stdout");
    output_end_results(&mut file, &end_results).expect("Error writing end results to file");
}

fn output_end_results(writer: &mut impl Write, end_results: &Vec<String>) -> io::Result<()> {
    write!(writer, "\n")?;
    write!(writer, "-------------------------------------------------\n")?;
    write!(writer, "-------------------------------------------------\n")?;
    write!(writer, "-------------------------------------------------\n")?;
    write!(writer, "\n\n")?;
    for line in end_results.iter() {
        writeln!(writer, "{}", line)?;
    }
    Ok(())
}

fn output(writer: &mut impl Write, tournaments: &[Tournament]) -> io::Result<f64> {
    if tournaments.len() == 0 {
        return Ok(0f64);
    }
    let mut writer = BufWriter::new(writer);
    writeln!(writer, "--------Rounds: {}, Losses To Eliminate: {}-------", tournaments[0].round_num, tournaments[0].max_loss)?;
    let mut sum_yield = vec![];
    
    for tournament in tournaments {
        writeln!(writer, 
            "Players: {}, Remain: {}, Yield: {}", 
            tournament.players.len(), 
            tournament.remaining_players, 
            tournament.r#yield(),
        )?;
        sum_yield.push(tournament.r#yield());
    }
    let avg: f64 = sum_yield.iter().sum::<f64>() / sum_yield.len() as f64;
    writeln!(writer, "Average Yield: {}\n\n", avg)?;
    Ok(avg)
}
