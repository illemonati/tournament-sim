

use super::player::Player;


#[derive(Debug)]
pub struct Match<'a> {
    pub players: &'a mut [Player],
    pub round_num: usize,
}

impl<'a> Match<'a> {
    pub fn new(players: impl Into<&'a mut [Player]>, round_num: usize) -> Match<'a> {
        Self { 
            round_num, 
            players: players.into(),
        }
    }

    pub fn run(&'a mut self) {
        if self.players.len() < 1 {
            return;
        }
        let win_index = 0;
        for (i, player) in self.players.iter_mut().enumerate() {
            if i == win_index {
                player.win_round_num(self.round_num);
            } else {
                player.lose_round_num(self.round_num);
            }
        }
         
    }
}

#[macro_export]
macro_rules! create_match {
    ($round_num:expr, $($player:expr),* ) => {
        {
            let mut players = Vec::new();
            $(
                players.push($player);
            )*
            Match::new(players, $round_num)
        }
    };
}