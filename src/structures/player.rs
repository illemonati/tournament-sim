
#[derive(Debug)]
pub struct Player {
    pub loss_count: usize,
    pub rounds_lost: Vec<usize>,
    pub final_round: usize,
    pub max_loss: usize,
}


impl Player {

    pub fn new(max_loss: usize) -> Self {
        Self {
            loss_count: 0,
            rounds_lost: Vec::new(),
            final_round: 0,
            max_loss: max_loss,
        }
    }

    pub fn win_round_num(&mut self, round_num: usize) {
        self.final_round += round_num;
    }

    pub fn lose_round_num(&mut self, round_num: usize) {
        self.final_round += round_num;
        self.loss_count += 1;
        self.rounds_lost.push(round_num);
    }

    pub fn is_eliminated(&self) -> bool {
        self.loss_count >= self.max_loss
    }
}