

use super::{player::Player, r#match::Match};

pub struct Round<'a> {
    pub round_num: usize,
    pub players: &'a mut [Player],
}

impl<'a> Round<'a> {
    pub fn new(round_num: usize, players: impl Into<&'a mut [Player]>) -> Round<'a> {
        Self {
            round_num,
            players: players.into(),
        }
    }

    pub fn run(&'a mut self) {
        for players in self.players.chunks_mut(2) {
            Match::new(players, self.round_num).run();
        }

    }

}