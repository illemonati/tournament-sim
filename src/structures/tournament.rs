use rand::{thread_rng, seq::SliceRandom};

use super::{round::Round, player::Player};

pub struct Tournament {
    pub players: Vec<Player>,
    pub round_num: usize,
    pub remaining_players: usize,
    pub max_loss: usize,
}

impl Tournament {
    pub fn new(round_num: usize, player_num: usize, max_loss: usize) -> Tournament {
        let mut players = Vec::new();
        for _ in 0..player_num {
            players.push(Player::new(max_loss));
        }
        Self {
            players,
            round_num,
            remaining_players: player_num,
            max_loss
        }
    }

    pub fn run(&mut self) {

        let mut eliminated_players: Vec<Player> = Vec::new();

        for r in 0..self.round_num {
            
            let mut round = Round::new(r, self.players.as_mut_slice());
            round.run();

            eliminated_players.extend(self.players.drain_filter(|player| player.is_eliminated()));
            self.players.shuffle(&mut thread_rng());
        }
        self.remaining_players = self.players.len();
        self.players.extend(eliminated_players);
    }

    pub fn r#yield(&self) -> f64 {
        self.remaining_players as f64/self.players.len() as f64
    }

  

}